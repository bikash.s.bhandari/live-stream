import React,{useState,useEffect,useRef} from 'react'
import { useParams } from "react-router-dom";
import videojs from 'video.js'
import axios from 'axios';
import config from '../../server/config/default';
import "video.js/dist/video-js.css";

const VideoPlayer1 = () => {
    const [stream, setStream] = useState(false);
    const [videoJsOptions, setVideoJsOptions] = useState(null);
    let params = useParams();
    const videoNode = useRef(null);
    const videoRef = React.useRef(null);
    const playerRef = React.useRef(null);
    
    useEffect(() => {
        axios.get('/user', {
            params: {
                username: params.username
            }
        }).then(res => {
            setStream(true);
            const url='http://127.0.0.1:' + config.rtmp_server.http.port + '/live/' + res.data.stream_key + '/index.m3u8'
            console.log({url})
            setVideoJsOptions({
                autoplay: true,
                controls: true,
                sources: [{
                    src: 'http://127.0.0.1:' + config.rtmp_server.http.port + '/live/' + res.data.stream_key + '/index.m3u8',
                    type: 'application/x-mpegURL'
                }],
                fluid: true,
            });

         
           
        })
       
    }, [])

    useEffect(() => {
    // make sure Video.js player is only initialized once
        if (!playerRef.current) {
          const videoElement = videoRef.current;
          if (!videoElement) return;
    
          const player = playerRef.current = videojs(videoElement, videoJsOptions, () => {
            console.log("player is ready");
            
            // onReady && onReady(player);
          });

        //   var player1 = videojs(videoElement, videoJsOptions, function onPlayerReady() {
        //     videojs.log('Your player is ready!');
          
        //     // In this context, `this` is the player that was created by Video.js.
        //     this.play();
          
        //     // How about an event listener?
        //     this.on('ended', function() {
        //       videojs.log('Awww...over so soon?!');
        //     });
        //   });
        } else {
          // you can update player here [update player through props]
          // const player = playerRef.current;
          // player.autoplay(options.autoplay);
          // player.src(options.sources);
        }
      }, [videoJsOptions, videoRef]);

    useEffect(() => {
        const player = playerRef.current;
    
        return () => {
          if (player) {
            player.dispose();
            playerRef.current = null;
          }
        };
      }, [playerRef]);
    
    return (
        
             <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-10 col-lg-8 mx-auto mt-5">
                    {stream ? (
                       <div data-vjs-player>
                        <video ref={videoRef} className="video-js vjs-big-play-centered" />
                      </div>
                    ) : ' Loading ... '}
                </div>
            </div>
            
        
    )
}

export default VideoPlayer1
