import React from "react";
import {Routes, Route} from 'react-router-dom';
import Navbar from './Navbar';
import LiveStreams from './LiveStreams';
import Settings from './Settings';
import VideoPlayer from './VideoPlayer1';
const customHistory = require("history").createBrowserHistory();

export default class Root extends React.Component {

    constructor(props){
        super(props);
    }

    render(){
        return (
            <div>
            <Navbar/>
            <Routes history={customHistory} >
                  
                    <Route path="/" element={<LiveStreams />} />
                    <Route
                    path="stream/:username"
                    element={<VideoPlayer  />}
                />
                <Route path="/settings" element={<Settings />} />

                  
                
            </Routes>
            </div>
        )
    }
}