
const _ = require('lodash');
const chokidar = require('chokidar');
const { join } = require('path');
const EventEmitter = require('events');
const fs = require('./fs');
const m3u8 = require('./m3u8');

const nodeEvent = new EventEmitter();

const on = (eventName, listener) => {
  nodeEvent.on(eventName, listener);
};

const VOD_APP_NAME = '720p';

// Function to create a unique VOD filename for each stream
const getVodName = (streams, streamName) => {
  if (!streams.has(streamName)) return false;
  return `vod-${streams.get(streamName)}.m3u8`;
};

// HLS - test4/720p/index.m3u8
const handlePlaylist = async (path, mediaRoot, streams, streamName, appName) => {
  console.log('handlePlaylist', path,mediaRoot);
  if (await fs.exists(path)) {

    console.log('exist here',streams,streamName)
    // Read 720p playlist
    // const liveM3u8 = await fs.readFile(join(mediaRoot, path));
    const liveM3u8 = await fs.readFile(path);



    // Put /vod.m3u8 with all segments and end tag.
    let vodM3u8;
    const vodFilename = getVodName(streams, streamName);
 
    if (vodFilename) {
      const vodPath = join(mediaRoot, streamName, vodFilename);
   

      if (await fs.exists(vodPath)) {
        // Read existing vod playlist.
        vodM3u8 = await fs.readFile(vodPath);
      } else {
        // New HLS Stream.
        console.log('emit newHlsStream event');
        nodeEvent.emit("newHlsStream", streamName);
      }
      vodM3u8 = m3u8.sync_m3u8(liveM3u8, vodM3u8, appName);
      await fs.writeFile(vodPath, vodM3u8);
   
    }
  }
};


// ABR - media/test4/live.m3u8
// HLS - media/test4/720p/index.m3u8
// TS  - media/test4/720p/20200504-1588591755.ts
// [360p, 480p, 720p]

const onFile = async (absolutePath, type, mediaRoot, streams) => {
  try {
    const path = _.trim(_.replace(absolutePath, mediaRoot, ''), '/');
    console.log('onFile',path)

    if (_.endsWith(path, '.ts')) {
  
      const paths = _.split(path, '/');
      const streamName = _.nth(paths, 3);
      const appName = _.nth(paths, 2);
      console.log({paths,streamName,appName})
    //   if (_.isEqual(appName, VOD_APP_NAME)) {
         await handlePlaylist(
          _.join(_.union(_.initial(_.split(path, '/')), ['index.m3u8']), '/'),
          mediaRoot,
          streams,
          streamName,
          appName);
    //   }
    }
  } catch (err) {
    console.log(err);
  }
};

const recordHls = (config, streams) => {

 const mediaRoot = config.http.mediaroot;
  chokidar.watch(mediaRoot, {
    ignored: /(^|[\/\\])\../, // ignore dotfiles
    persistent: true,
    ignoreInitial: true,
    awaitWriteFinish: {
      stabilityThreshold: 6000,
      pollInterval: 100
    }
  }).on('add', (path) => onFile(path, 'add', mediaRoot, streams))
    .on('change', (path) => onFile(path, 'change', mediaRoot, streams));
};

module.exports = {
  recordHls,
  on
};