require("dotenv").config();
const express = require('express');
const session = require('express-session');
const bodyParse = require('body-parser');
const mongoose = require("mongoose");
const middleware = require('connect-ensure-login');
const config = require('./config/default');
const flash = require('connect-flash');
const path=require('path');
const passport = require('./auth/passport');
const MongoStore = require('connect-mongo');
const port = config.server.port;
const node_media_server = require('./media_server');
const thumbnail_generator = require('./cron/thumbnails');
const logger =require('./lib/logger');
const app = express();
const LOG_TYPE = 4;
logger.setLogType(LOG_TYPE);
mongoose.connect(
    "mongodb://localhost:27017/livestream",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
    (err) => {
      if (err) throw err;
      console.log("Connected to mongodb");
    }
  );
 
  app.set('view engine', 'ejs');
  app.set('views', path.join(__dirname, './views'));
  app.use(express.static('public'));
  app.use('/thumbnails', express.static('server/thumbnails'));
  app.use(flash());
  
  app.use(require('cookie-parser')());
  app.use(bodyParse.urlencoded({extended: true}));
  app.use(bodyParse.json({extended: true}));
  
  app.use(session({
      store: MongoStore.create({
          mongoUrl: 'mongodb://localhost:27017/livestream',
          ttl: 14 * 24 * 60 * 60 // = 14 days. Default
      }),
      secret: config.server.secret,
      maxAge : Date().now + (60 * 1000 * 30),
      resave : true,
      saveUninitialized : false,
  }));
  
  app.use(passport.initialize());
  app.use(passport.session());


 
// Register app routes
app.use('/login', require('./routes/login'));
app.use('/register', require('./routes/register'));
app.use('/settings', require('./routes/settings'));
app.use('/streams', require('./routes/streams'));
app.use('/user', require('./routes/user'));

app.get('/logout', (req, res) => {
    req.logout();
    return res.redirect('/login');
});

app.get('*', middleware.ensureLoggedIn(), (req, res) => {
    res.render('index');
});

app.listen(port, () => console.log(`App listening on ${port}!`));
thumbnail_generator.start();
node_media_server.run();