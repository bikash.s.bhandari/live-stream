const NodeMediaServer = require('node-media-server'),
    config = require('./config/default').rtmp_server,
    User = require('./database/Schema').User,
    helpers = require('./helpers/helpers');
const hls = require('./lib/hls');
const logger = require('./lib/logger');
const utils=require('./lib/utils')
const cache=require('./lib/cache')
const abr=require('./lib/abr')
const _ = require('lodash');

const SERVER_ADDRESS = '';

nms = new NodeMediaServer(config);
this.dynamicSessions = new Map();
this.streams = new Map();

hls.recordHls(config, this.streams);

hls.on('newHlsStream', async (name) => {
    // Create the ABR HLS playlist file.
    await abr.createPlaylist(config.http.mediaroot, name);
    await cache.set(name, SERVER_ADDRESS);
  });



nms.on('prePublish', async (id, StreamPath, args) => {
    let stream_key = getStreamKeyFromStreamPath(StreamPath);
    console.log('[NodeEvent on prePublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);

    User.findOne({stream_key: stream_key}, (err, user) => {
        if (!err) {
            if (!user) {
                let session = nms.getSession(id);
                session.reject();
            } else {
                helpers.generateStreamThumbnail(stream_key);
            }
        }
    });
});

nms.on('postPublish', async (id, StreamPath, args) => {
    console.log('post publish here',StreamPath)
    logger.log('[NodeEvent on postPublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
    if (StreamPath.indexOf('/live/') != -1) {
      // Set the "stream key" <-> "id" mapping for this RTMP/HLS session
        const name = StreamPath.split('/').pop();
        this.streams.set(name, id);
     
      
    } else if (StreamPath.indexOf('/stream/') != -1) {
       console.log('for facebook')
      
     
    
    }
  });
  
  nms.on('donePublish', async (id, StreamPath, args) => {
    logger.log('[NodeEvent on donePublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
    if (StreamPath.indexOf('/live/') != -1) {
      const name = StreamPath.split('/').pop();

      // Wait a few minutes before deleting the HLS files on this Server
      // for this session
      const timeoutMs = _.isEqual(process.env.NODE_ENV, 'development') ?
        1000 : 
        2 * 60 * 1000;
       await utils.timeout(timeoutMs);
    //   if (!_.isEqual(await cache.get(name), SERVER_ADDRESS)) {
    //     // Only clean up if the stream isn't running.  
    //     // The user could have terminated then started again.
    //     try {
    //       // Cleanup directory
    //       logger.log('[Delete HLS Directory]', `dir=${join(config.rtmp_server.http.mediaroot, name)}`);
    //       this.streams.delete(name);
    //       fs.rmdirSync(join(config.rtmp_server.http.mediaroot, name));
    //     } catch (err) {
    //       logger.error(err);
    //     }
    //   }
    } else if (StreamPath.indexOf('/stream/') != -1) {
        console.log('stop relays')
    
     
      
    
    }
  });

const getStreamKeyFromStreamPath = (path) => {
    let parts = path.split('/');
    return parts[parts.length - 1];
};

module.exports = nms;